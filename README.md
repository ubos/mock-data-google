# Google export file analysis, and mock Google data

Nobody wants to share the entirety of their Google data export with others.
This makes it difficult to develop, test, or demonstrate code that can
read Google data exports. It doesn't help that Google hasn't documented
their export format.

This repo contains :

* a file-by-file [analysis](format-analysis/) of the structure and meaning
  of the Google data export file, as well as we have been able to
  explore and understand it;

* an [example instantiation](william-smith/) of that data for a fictional
  character called William Smith.

**This is extremely early stage** and covers only a tiny fraction of Google
at this time.

**HELP WANTED**: Please [download your own data](https://takeout.google.com/)
from Google, and compare it against our documentation here.
If you find some data elements that we don't have documented, or some
other values or data structures that are different from our documentation,
please tell us!

Best is with a pull request to the format analysis and an extension of the
mock data, so we have better coverage.  Second best is to file an issue,
copy-pasting the data that shows the issue (after you change any
personally-identifyable information to something harmless).


## Pre-built mock data files

Pre-built mock data files are at http://depot.ubos.net/mockdata/google/

## How to build the mock data:

```
% make
```

## How to create a mock data "release":

```
% TAG=v0.0.1 make release
```
and then upload as a release.
