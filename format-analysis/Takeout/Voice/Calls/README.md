Apparently one or two files for each call that was saved

Filename format: "NUMBER - TYPE - DATE.EXT" where

* NUMBER: phone number in full + country format, or blank if not known

* TYPE: one of:

  * `Voicemail`

  * `Missed`

  * `Received`

* DATE: YYYY-MM-DDTHH_MM_SSZ

* EXT: either `html` or `mp3`.
