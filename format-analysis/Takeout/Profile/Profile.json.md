JSON file with:

* `name`:

  * `givenName` : string

  * `familyName` : string

  * `formattedName` : string

* `displayName`

* `emails`: array of

  * `value`: e-mail address

* `organizations` : array of

  * `name`: string

  * `title`: string. This apparently can be a job title or a degree being
    pursued

  * `startDate` (optional): string

  * `endDate` (optional): string

  * `type`: enum? Known values:

    * `school`

    * `work`

  * `primary` (optional): boolean

* `birthday`: string

* `gender` :

  * `type` : enum? Known values:

    * `male`

* `urls`: array of:

  * `value`: the URL

  * `label`: string

  * `type` (optional): enum? Known values:

    * `otherProfile`
