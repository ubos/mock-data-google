#!/usr/bin/python
#
# Script to generate an index.html suitable for Amazon S3 buckets

import os

print( """<!DOCTYPE html>
<html lang="en">
 <head>
 </head>
 <body>
  <h1>Available mock data files:</h1>
  <ul>""" )

for f in sorted( os.listdir( '.' )) :
    if f != "index.html" :
        print( f"   <li><a href=\"{f}\">{f}</a></li>" )

print( """  </ul>
 </body>
</html>
""" )

