# Create the mock export data zip

default : generated/google-williamsmith-20200113.zip

# normal build
generated/google-williamsmith-20200113.zip : .FORCE
	[[ -d generated ]] || mkdir generated
	[[ -e $@ ]] && rm $@ || true
	cd william-smith/data-20200113 && zip -r ../../$@ *

# release build
release :
ifndef TAG
	$(error Must provide a TAG var, e.g. "TAG=v0.0.1 make release")
endif
	git tag -a "$(TAG)"
	[[ -d releases ]] || mkdir releases
	cd william-smith/data-20200113 && zip -r ../../releases/google-williamsmith-20200113-${TAG}.zip *

upload :
	[[ -d uploads ]] || mkdir uploads
	cp releases/* uploads/
	cp `ls -1t releases/google-williamsmith-20200113-*.zip | head -1` uploads/google-williamsmith.zip
	cd uploads && python ../bin/generate-index.py > index.html
	aws s3 sync --delete --acl public-read uploads s3://depot.ubos.net/mockdata/google

clean :
	rm generated/* uploads/*

.PHONY: default release upload clean

.FORCE:

